const concat = require("./concat");

test("Should concatenate two strings with a space between them", () => {
  expect(concat("Hello", "World")).toBe("Hello World");
});
