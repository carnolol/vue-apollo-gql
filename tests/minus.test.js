const minus = require("./minus");

test("tests 100 - 1 to equal 99", () => {
  expect(minus(100, 1)).toBe(99);
});
